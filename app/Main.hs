
import Data.Foldable
import System.Environment

import Error
import Eval
import Parser
import Pretty
import Prog
import Typecheck

main :: IO ()
main = do
  getArgs >>= \case
    [fin] -> main' fin
    _     -> putStrLn "USAGE: arrowhead <file>"

main' :: String -> IO ()
main' fin = do
  txt  <- readFile fin
  prog <- run (parser @Prog) fin
  case prog of
    Just it -> do
      let scope = scopeCheck it `runSC` []
      for_ scope (`printError` txt)
      if lethal scope
      then do
        return ()
      else do
        case typecheck it [] of
          Left errs -> do
            for_ errs \err -> do
              let (place, severity) = report err
              putStrLn $ show severity ++ " at " ++ show place ++ " - " ++ show err
          Right ty -> do
            print $ pp it
            putStrLn "  :"
            print $ pp ty
    Nothing -> do
      return ()
