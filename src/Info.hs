
module Info where

import Pretty

data Pos = Pos { posCol :: Int, posRow :: Int }
  deriving stock (Eq, Ord)
  deriving Show via PP Pos

data Info = Info { infoFrom :: Pos, infoTo :: Pos, infoFile :: String }
  deriving stock (Eq, Ord)
  deriving Show via PP Info

instance Pretty Info where
  pp Info {infoFrom, infoTo, infoFile} = text infoFile <.> "," <+> pp infoFrom <.> "-" <.> pp infoTo

instance Pretty Pos where
  pp Pos {posCol, posRow} = int posRow <.> ":" <.> int posCol

nowhere :: Info
nowhere = Info (Pos (-1) (-1)) (Pos (-1) (-1)) "-"

infoStart :: Info -> Doc
infoStart Info { infoFrom, infoFile} = text infoFile <.> ":" <.> pp infoFrom