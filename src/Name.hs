
module Name where

import Control.Comonad.Cofree
import Data.Monoid
import Data.String

import Pretty
import Info

type Name = Cofree Name_ Info
data Name_ self = Name { nameRaw :: String }
  deriving stock (Eq, Ord)
  deriving stock (Functor, Foldable, Traversable)

instance Pretty (Name_ Name) where
  pp = text . nameRaw

instance IsString Name where
  fromString n = nowhere :< Name n

eqName :: Name -> Name -> Bool
eqName (_ :< n) (_ :< m) = n == m

lookupBy :: (a -> a -> Bool) -> a -> [(a, b)] -> Maybe b
lookupBy eq a =
  getFirst . foldMap \(a', b) ->
    if eq a a'
    then return b
    else First Nothing
