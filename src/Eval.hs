
module Eval where

import Control.Comonad
import Control.Comonad.Cofree
import Control.Monad.Reader
import Control.Monad.Writer
import Data.Maybe
import Data.Foldable

import Prog
import Error
import Name
import Info
import Pretty

import Debug.Trace ()

data ScopeProblem
  = Undefined    Name
  | Shadowing    Name Info
  | Unused       Name
  | CaptureUndef Name
  deriving Show via PP ScopeProblem

instance Severe ScopeProblem where
  report (Undefined    n)   = (extract n, Error)
  report (Shadowing    n _) = (extract n, Warning)
  report (Unused       n)   = (extract n, Warning)
  report (CaptureUndef n)   = (extract n, Error)

type Context = [Name]

type SC = ReaderT Context (Writer [ScopeProblem])

runSC :: SC () -> Context -> [ScopeProblem]
runSC sc ctx = execWriter $ runReaderT sc ctx

findName :: Name -> SC (Maybe Name)
findName n = do
  ctx <- ask
  return $ listToMaybe $ filter (eqName n) ctx

class HasScope p where
  scopeCheck :: p -> SC ()

instance HasScope a => HasScope (Maybe a) where
  scopeCheck = maybe (return ()) scopeCheck

instance HasScope Prog where
  scopeCheck = with \_ -> \case
    Var v -> do
      exists <- findName v
      tell [Undefined v | isNothing exists]

    App f xs -> do
      for_ (f : xs) scopeCheck

    Lam az b -> do
      for_ az \(n, _) -> do
        shadows n
        unused n [b]

      local (map fst az ++) do
        scopeCheck b

    If bs -> for_ bs scopeCheck

    Let ds b -> do
      delta <- letrec True (undeclare =<< ds) (Just b)
      local (delta ++) do
        scopeCheck b

    Const {} -> return ()

    As p _ -> do
      scopeCheck p

    Rec fs -> do
      _ <- letrec False (unreclare =<< fs) Nothing
      return ()

    Get p _ -> do
      scopeCheck p

    Set p fs -> do
      scopeCheck p
      _ <- letrec False (unreclare =<< fs) Nothing
      return ()

letrec :: Bool -> [(Name, Maybe Prog)] -> Maybe Prog -> SC [Name]
letrec checkUnused frame k = do
  let (delta, bodies) = unzip frame
  for_ frame \(n, p) -> do
    case p of
      Nothing -> do
        exists <- findName n
        tell [CaptureUndef n | isNothing exists]

      Just _ -> do
        shadows n
        when checkUnused do
          unused n (bodies ++ [k])

  local (delta ++) do
    for_ bodies scopeCheck

  return delta

shadows :: Name -> SC ()
shadows n = do
  exists <- findName n
  tell case exists of
    Nothing -> []
    Just m  -> [Shadowing n (extract m)]

unused :: (UsesName p, Pretty p) => Name -> [p] -> SC ()
unused n bs = do
  tell [Unused n | all (isUnused n) bs]

instance HasScope IfBranch where
  scopeCheck = with \_ -> \case
    Then b y -> do
      for_ [b, y] scopeCheck

undeclare :: Decl -> [(Name, Maybe Prog)]
undeclare (_ :< Decl n _ b) = [(n, Just b)]
undeclare  _                = []

unreclare :: RecDecl -> [(Name, Maybe Prog)]
unreclare (_ :< Field n b) = [(n, Just b)]
unreclare (_ :< Capture n) = [(n, Nothing)]

declName :: Decl -> [Name]
declName (_ :< Decl n _ _) = [n]
declName  _                = []

declBody :: Decl -> [Prog]
declBody (_ :< Decl _ _ b) = [b]
declBody  _                = []

class UsesName p where
  isUnused :: Name -> p -> Bool

instance UsesName a => UsesName (Maybe a) where
  isUnused n = maybe True (isUnused n)

instance UsesName Prog where
  isUnused n = with \_ -> \case
    Var v    -> not $ eqName v n
    App f xs -> all (isUnused n) (f : xs)
    Lam az b -> isJust (lookupBy eqName n az) || isUnused n b
    If  bs   -> all (isUnused n) bs
    Let ds b -> any (hide n) ds || all (isUnused n) ds && isUnused n b
    Const {} -> True
    As  p _  -> isUnused n p
    Rec fs   -> all (isUnused n) fs
    Get p _  -> isUnused n p
    Set p fs -> isUnused n p && all (isUnused n) fs

hide :: Name -> Decl -> Bool
hide n (_ :< Decl n' _ _) = eqName n n'
hide _ (_ :< Type {})     = False


instance UsesName IfBranch where
  isUnused n (_ :< Then y b) = isUnused n y && isUnused n b

instance UsesName Decl where
  isUnused n (_ :< Decl n' _ b) = eqName n n' || isUnused n b
  isUnused _ (_ :< Type {})    = True

instance UsesName RecDecl where
  isUnused n (_ :< Field n' b) = eqName n n' || isUnused n b
  isUnused n (_ :< Capture n') = not (eqName n n')

instance Pretty ScopeProblem where
  pp (Undefined    n  ) = "undefined" <+> pp n
  pp (Shadowing    n j) = pp n <+> "shadows another declaration at" <+> pp (infoStart j)
  pp (Unused       n  ) = pp n <+> "is unused"
  pp (CaptureUndef n  ) = "cannot capture" <+> pp n <.> ", it is undefined"
