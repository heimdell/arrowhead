
{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Lexer (module Lexer) where

import Control.Monad.Identity
import Text.Parsec.Token
import Text.ParserCombinators.Parsec

TokenParser {
  identifier,
  reserved,
  operator,
  reservedOp,
  charLiteral,
  stringLiteral,
  natural,
  integer,
  float,
  naturalOrFloat,
  decimal,
  hexadecimal,
  octal,
  symbol,
  lexeme,
  whiteSpace,
  parens,
  braces,
  angles,
  brackets,
  semi,
  comma,
  colon,
  dot,
  semiSep,
  semiSep1,
  commaSep,
  commaSep1
  } = tokenParser

tokenParser :: GenTokenParser String () Identity
tokenParser = makeTokenParser LanguageDef
  { commentStart    = "{-"
  , commentEnd      = "-}"
  , commentLine     = "--"
  , nestedComments  = True
  , identStart      = oneOf $ ['A'.. 'Z'] ++ ['a'.. 'z'] ++ "_"
  , identLetter     = oneOf $ ['A'.. 'Z'] ++ ['a'.. 'z'] ++ "_" ++ ['0'.. '9'] ++ "-?!'"
  , opStart         = oneOf "!@#$%^&|*-+=?/\\:~.,"
  , opLetter        = oneOf "!@#$%^&|*-+=?/\\:~.,"
  , reservedNames   = words "let in if fun end type true false as"
  , reservedOpNames = words "-> => : = |"
  , caseSensitive   = True
  }

tok :: String -> Parser ()
tok s = try (string s) *> (spaces <?> "")
