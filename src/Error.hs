
module Error where

import Info
import Pretty

import Debug.Trace ()

data Severity
  = Notice
  | Warning
  | Error
  deriving stock (Eq, Show)

class Severe e where
  report :: e -> (Info, Severity)

assert :: String -> Maybe a -> a
assert msg = maybe (error msg) id

lethal :: Severe e => [e] -> Bool
lethal = any (Error ==) . map (snd . report)

printError :: (Severe e, Pretty e) => e -> String -> IO ()
printError e@(report -> (Info {infoFrom, infoFile}, severity)) txt = do
  let ls    = lines txt
  let line  = concat $ take 1 $ drop (posRow infoFrom - 1) ls
  let pre   = infoFile ++ ":" ++ show (posRow infoFrom) ++ ":" ++ show (posCol infoFrom) ++ " | "
  let caret = replicate (posCol infoFrom - 1 + length pre) ' ' ++ "^"
  let msgs  = pp e
  putStrLn (pre ++ line)
  putStrLn caret
  print $ text (show severity) <.> ":" `indent` msgs
  putStrLn ""
  putStrLn "----"
  putStrLn ""
