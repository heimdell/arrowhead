
module Typecheck where

import Control.Arrow (second)
import Control.Comonad
import Control.Comonad.Cofree
import Control.Monad.Reader
import Control.Monad.Writer
import Control.Monad.Except
import Control.Monad.State

import Data.Traversable
import Data.String

import Prog
import Info
import Error
import Name
import Pretty

import Debug.Trace

data TypeError
  = Mismatch Type Type
  | Expected Prog Type Type
  | Cycle    Name
  | ExpectedContain Prog Name Type
  deriving Show via PP TypeError

instance Severe TypeError where
  report (Mismatch t _  ) = (extract t, Error)
  report (Expected p _ _) = (extract p, Error)
  report (Cycle    n    ) = (extract n, Error)
  report (ExpectedContain p _ _) = (extract p, Error)

instance Pretty TypeError where
  pp = \case
    Mismatch l r ->
      "types"          `indent` pp l `above`
      "and"            `indent` pp r `above`
      "don't match"

    Expected p t ty ->
      "expected program" `indent` nm (pp p) `above`
      "to have type"     `indent` pp t  `above`
      "but it is"        `indent` pp ty `above`
      "instead"

    Cycle n ->
      "type alias" `indent` pp n `above`
      "is (probaly indirectly) cyclic"

    ExpectedContain p f t ->
      "expected program"            `indent` nm (pp p) `above`
      "to return record with field" `indent` pr (pp f) `above`
      "but it is"                   `indent` pp t `above`
      "instead"

type TC = ReaderT Context (StateT Int (WriterT Subst (Except [TypeError])))

type Context = [(Name, Type)]
type Subst   = [(Name, Type)]

fresh :: TC Type
fresh = do
  s <- get
  modify (+ 1)
  return $ nowhere :< TVar (fromString $ "a" ++ show s)

typecheck :: Prog -> Context -> Either [TypeError] Type
typecheck prog ctx = do
  (ty, s) <- runExcept $ runWriterT $ flip evalStateT 0 $ runReaderT (typeOf prog) ctx
  ns <- normalise s
  traceShowM (pp ns)
  return (substTypes ns ty)

-- runTC :: TC a -> Context -> Either [TypeError] a
-- runTC tc ctx = runExcept $ runReaderT tc ctx

typeOfName :: Name -> TC Type
typeOfName n = do
  (assert "scopeCheck" . lookupBy eqName n) <$> ask

makeArr :: Info -> [Type] -> Type -> Type
makeArr i [] t = t $> i
makeArr i (nt : rest) t = i :< Arr nt (makeArr i rest t)

class HasType p where
  typeOf_ :: p -> TC Type

typeOf :: (Functor f, HasType (Cofree f Info)) => Cofree f Info -> TC Type
typeOf f = do
  _ :< t <- typeOf_ f
  return $ extract f :< t

makeTelescope :: Type -> [Type] -> Type
makeTelescope = foldr (\l r -> nowhere :< Arr l r)

instance HasType Prog where
  typeOf_ = with \i -> \case
    Var v -> typeOfName v
    App f xs -> do
      (tf, txs) <- typeOf f <-> collect typeOf xs
      n <- fresh
      let tf' = makeTelescope n txs
      unify tf tf'
      return n

    Lam bs b -> do
      local (bs <>) do
        tb <- typeOf b
        return (makeArr i (map snd bs) tb)

    If bs -> do
      ~tbs@(tb1 : _) <- collect typeOf bs
      _ <- collect (`unify` tb1) tbs
      return tb1

    Let ds0 b0 -> do
      let renames = rename =<< ds0
      renames' <- normalise renames
      let ds    = map (substTypes renames') ds0
      let b     =      substTypes renames'  b0
      delta <- letrec ds
      local (delta ++) do
        typeOf b

    As p t -> do
      expected p t

    Const c -> typeOf c

    Rec fs -> do
      typed <- letrec fs
      return (i :< TRec typed)

    Get p f -> do
      tp <- typeOf p
      case tp of
        _ :< TRec (lookupBy eqName f -> Just ty) -> do
          return ty

        other -> throwError [ExpectedContain p f other]

    Set p fs -> do
      tp    <- typeOf p
      typed <- letrec fs
      return $ tp `union` (i :< TRec typed)

union :: Type -> Type -> Type
union (_ :< TRec fs) (i :< TRec gs) =
  i :< TRec (gs ++ filter (notPresentIn gs) fs)
  where
    notPresentIn (map fst -> names) (n, _) =
      null $ filter (eqName n) names
union l r = error $ show $ "union (" <.> pp l <.> "," <+> pp r <.> ")"

class IsDecl d where
  undeclare :: d -> [(Name, Maybe Prog)]

declName :: IsDecl d => d -> [Name]
declName = map fst . undeclare

instance IsDecl RecDecl where
  undeclare (_ :< Field n b) = [(n, Just b)]
  undeclare (_ :< Capture n) = [(n, Nothing)]

instance IsDecl Decl where
  undeclare (_ :< Decl n _ b) = [(n, Just b)]
  undeclare (_ :< Type {})    = []

letrec :: IsDecl d => [d] -> TC [(Name, Type)]
letrec frame = do
  let ds = frame >>= undeclare
  delta <- for ds \(n, _) -> do
    t <- fresh
    return (n, t)

  _ <- local (delta ++) do
    for ds \(n, p) -> do
      maybe (typeOfName n) typeOf p

  return delta

instance HasType IfBranch where
  typeOf_ = with \_ -> \case
    Then y b ->
      snd <$> expected y (nowhere :< T "bool") <-> typeOf b

expected :: Prog -> Type -> TC Type
expected p ty = do
  tp <- typeOf p
  unify tp ty
  return tp

instance HasType Const where
  typeOf_ = with \i -> \case
    Bool   {} -> return (i :< T "bool")
    Int    {} -> return (i :< T "int")
    Float  {} -> return (i :< T "float")
    String {} -> return (i :< T "string")
    Char   {} -> return (i :< T "char")

undecl :: Decl -> [(Name, Type)]
undecl (_ :< Decl n t _) = [(n, t)]
undecl (_ :< Type {})    = []

rename :: Decl -> [(Name, Type)]
rename (_ :< Type n t) = [(n, t)]
rename (_ :< Decl {})  = []

class SubstType s where
  subst :: Name -> Type -> s -> s

substTypes :: SubstType s => [(Name, Type)] -> s -> s
substTypes = flip (foldr (uncurry subst))

instance SubstType Prog where
  subst n t = go
    where
      go = withGist \case
        Lam bs b -> Lam (map (second $ subst n t) bs) (go b)
        App f x  -> App (go f) (map (subst n t) x)
        If bs    -> If (map (subst n t) bs)
        Var x    -> Var x
        Let ds b -> case lookupBy eqName n (ds >>= rename) of
          Just _ -> Let ds b
          Nothing -> Let (map (subst n t) ds) (go b)
        As p ty  -> As (go p) (subst n t ty)
        Const c  -> Const c
        Rec fs   -> Rec (map (subst n t) fs)
        Get p f  -> Get (subst n t p) f
        Set p fs -> Set (subst n t p) (map (subst n t) fs)

instance SubstType RecDecl where
  subst n t = withGist \case
    Field n' p -> Field n' (subst n t p)
    Capture n' -> Capture n'

instance SubstType Type where
  subst n t = withGist \case
    T       n'    -> if eqName n n' then Renamed n' t else T n'
    Arr     l  r  -> Arr (subst n t l) (subst n t r)
    Renamed n' t' -> Renamed n' (subst n t t')
    TRec    fs    -> TRec (map (second (subst n t)) fs)
    TVar    a     -> TVar a

instance SubstType Decl where
  subst n t = withGist \case
    Decl a ty b -> Decl a (subst n t ty) (subst n t b)
    Type a ty   -> Type a (subst n t ty)

instance SubstType IfBranch where
  subst n t = withGist \case
    Then b y -> Then (subst n t b) (subst n t y)

normalise :: MonadError [TypeError] m => [(Name, Type)] -> m [(Name, Type)]
normalise [] = return []
normalise ((n, t) : rest) = do
  case occurs n t of
    Just m  -> throwError [Cycle m]
    Nothing -> do
      ((n, t) :) <$> normalise (map (second $ subst n t) rest)

occurs :: Name -> Type -> Maybe Name
occurs n = getFirst . go
  where
    go = with \_ -> \case
      T       n'   -> First if eqName n n' then Just n' else Nothing
      Arr     l  r -> go l <> go r
      Renamed n' t -> if eqName n n' then return n' else go t
      TRec    fs   -> foldMap (go . snd) fs
      TVar    _    -> First Nothing

occursVar :: Name -> Type -> Maybe Name
occursVar n = getFirst . go
  where
    go = with \_ -> \case
      TVar    n'   -> First if eqName n n' then Just n' else Nothing
      Arr     l  r -> go l <> go r
      Renamed n' t -> if eqName n n' then return n' else go t
      TRec    fs   -> foldMap (go . snd) fs
      T       _    -> First Nothing

instance HasType Decl where
  typeOf_ = with \_ -> \case
    Decl _ t b -> do
      tb <- typeOf b  -- ctx already has (n, t)
      unify tb t
      return t
    Type _ t ->
      return t

(<->) :: TC a -> TC b -> TC (a, b)
l <-> r = do
  l' <- try l
  r' <- try r
  liftEither (l' <--> r')
  where
    Left  l' <--> Left  r' = Left (l' <> r')
    Left  l' <--> _        = Left l'
    _        <--> Left  r' = Left r'
    Right a  <--> Right b  = Right (a, b)

try :: MonadError e m => m a -> m (Either e a)
try ma = do
  (Right <$> ma) `catchError` (return . Left)

collect :: (a -> TC b) -> [a] -> TC [b]
collect f = \case
  [] -> return []
  x : xs -> do
    (x', xs') <- f x <-> collect f xs
    return (x' : xs')

-- eqType :: Type -> Type -> Bool
-- eqType = (?=) `on` gist
--   where
--     (?=) :: Type_ Type -> Type_ Type -> Bool
--     T       x   ?= T       y   = eqName x y
--     Arr     a b ?= Arr     c d = eqType a c && eqType b d
--     Renamed _ t ?= Renamed _ u = eqType t u
--     Renamed _ t ?= u           = gist t ?= u
--     t           ?= Renamed _ u = t ?= gist u
--     _           ?= _           = False

unify :: Type -> Type -> TC ()
unify   (_ :< TVar a)      c@(_ :< TVar b)      = tell [(a, c) | not $ eqName a b]
unify   (_ :< TVar a)      c               | Just x <- occursVar a c = throwError [Cycle x]
unify    c                   (_ :< TVar a) | Just x <- occursVar a c = throwError [Cycle x]
unify   (_ :< TVar a)      c                    = tell [(a, c)]
unify    c                   (_ :< TVar a)      = tell [(a, c)]
unify   (_ :< Arr l r)       (_ :< Arr m s)     = unify l m >> unify r s
unify   (_ :< Renamed _ t)   (_ :< Renamed _ u) = unify t u
unify   (_ :< Renamed _ t) u                    = unify t u
unify    t                   (_ :< Renamed _ u) = unify t u
unify t@(_ :< T n)         u@(_ :< T m)         = unless (eqName n m) do throwError [Mismatch t u]
unify    t                 u                    = throwError [Mismatch t u]
