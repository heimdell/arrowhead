
module Parser where

import Control.Applicative hiding (Const)
import Control.Comonad.Cofree
import Text.ParserCombinators.Parsec hiding (many, (<|>))
import Text.Parsec.Error

import Prog
import Name
import Info
import Error
import Pretty hiding (char)

import qualified Lexer

class Parse x where
  parser :: Parser x

instance Parse (f (Cofree f Info)) => Parse (Cofree f Info) where
  parser = lifted parser

lifted :: Parser (f (Cofree f Info)) -> Parser (Cofree f Info)
lifted = positioned . (flip (:<) <$>)

positioned :: Parser (Info -> a) -> Parser a
positioned p = do
    from <- parser
    fun  <- p
    to   <- parser
    i    <- getPosition
    return $ fun Info
      { infoFrom = from
      , infoTo   = to
      , infoFile = sourceName i
      }

instance Parse Pos where
  parser = do
    i <- getPosition
    return Pos
      { posRow = sourceLine   i
      , posCol = sourceColumn i
      }

instance Parse (Name_ Name) where
  parser = Name <$> Lexer.identifier <?> "name"

instance Parse (Type_ Type) where
  parser = arrow
    where
      term :: Parser (Type_ Type)
      term = choice
        [ T <$> parser
        , Lexer.parens (gist <$> parser)
            <?> "type in parentheses"
        ]
        <?> "type"

      arrow :: Parser (Type_ Type)
      arrow = do
        dom <- lifted term
        cod <- optionMaybe do
          Lexer.reservedOp "->" <?> "arrow operator"
          parser
        return $ maybe (gist dom) (Arr dom) cod

instance Parse (Prog_ Prog) where
  parser = app
    where
      app = do
        fxs <- lifted do
          f  <- lifted term
          xs <- many (lifted term <?> "arg")

          return $ App f xs

        ass <- optionMaybe do
          Lexer.reserved "as"
          parser

        return $ maybe gist (flip As) ass fxs

      term = choice
        [ Var <$> parser
        , Lexer.parens (gist <$> parser)
            <?> "expression in parentheses"
        , Const <$> parser
        , ifExpr
        , lamExpr
        , letExpr
        ]

      letExpr = do
        Lexer.reserved "let"
        ds <- Lexer.semiSep1 parser
        Lexer.reserved "in"
        b <- parser
        Lexer.reserved "end"
        return (Let ds b)

      ifExpr = do
        Lexer.reserved "if"
        bs <- some parser
        Lexer.reserved "end"
        return (If bs)

      lamExpr = do
        Lexer.reserved "fun"
        args <- some do
          Lexer.parens do
            n <- parser
            Lexer.reservedOp ":"
            t <- parser
            return (n, t)
        Lexer.reservedOp "=>"
        b <- parser
        Lexer.reserved "end"
        return $ Lam args b

instance Parse (IfBranch_ IfBranch) where
  parser = do
    Lexer.reservedOp "|"
    b <- parser
    Lexer.reservedOp "->"
    y <- parser
    return (Then b y)

instance Parse (Decl_ Decl) where
  parser = choice [type_, fun]
    where
      fun = do
        n <- parser
        Lexer.reservedOp ":"
        t <- parser
        Lexer.reservedOp "="
        b <- parser
        return $ Decl n t b

      type_ = do
        Lexer.reserved "type"
        n <- parser
        Lexer.reservedOp "="
        t <- parser
        return $ Type n t

instance Parse (Const_ Const) where
  parser = choice [booleanL, integerL, stringL, floatL, charL]
    where
      booleanL = choice
        [ Lexer.reserved "true"  *> return (Bool True)
        , Lexer.reserved "false" *> return (Bool False)
        ]

      integerL = Int    <$> try Lexer.integer
      stringL  = String <$> Lexer.stringLiteral
      charL    = Char   <$> Lexer.charLiteral
      floatL   = Float  <$> try Lexer.float

data ParserError
  = Expected String Info
  | NotExpected     Info

instance Severe ParserError where
  report (Expected _  i) = (i, Error)
  report (NotExpected i) = (i, Error)

instance Pretty ParserError where
  pp = \case
    Expected s _ -> "expected" <+> text s
    NotExpected _ -> "not expected"

prettyMessages :: ParseError -> ParserError
prettyMessages pe
  = decide
  $ punct
  $ map messageString
  $ filter isExpected
  $ errorMessages pe
  where
    isExpected Expect {} = True
    isExpected _         = False

    punct [] = Nothing
    punct [x] = return x
    punct [x, y] = return (x ++ " or " ++ y)
    punct (x : xs) = do
      rest <- punct xs
      return (x ++ ", " ++ rest)

    decide = ($ info) . maybe NotExpected Expected

    info = Info
      { infoFrom = place
      , infoTo   = place
      , infoFile = sourceName pos
      }

    place = Pos { posRow = sourceLine pos, posCol = sourceColumn pos }

    pos = errorPos pe

deriving instance Show Message

run :: Parser a -> String -> IO (Maybe a)
run p file = do
  txt <- readFile file
  case parse (Lexer.whiteSpace *> p <* eof) file txt of
    Left err -> printError (prettyMessages err) txt *> return Nothing
    Right a  -> return (Just a)
