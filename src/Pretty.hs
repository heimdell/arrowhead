
module Pretty (module Text.PrettyPrint, module Pretty) where

import Text.PrettyPrint hiding ((<>))

class Pretty p where
  pp :: p -> Doc

class Functor p => Pretty1 p where
  pp1 :: p Doc -> Doc

instance Pretty Doc where
  pp = id

instance Pretty a => Pretty [a] where
  pp = brackets . fsep . punctuate comma . map pp

instance (Pretty a, Pretty b) => Pretty (a, b) where
  pp (a, b) = "(" <.> pp a <.> "," <+> pp b <.> ")"

instance Pretty a => Pretty (Maybe a) where
  pp = maybe "Nothing" (("Just" <+>) . pp)

newtype PP a = PP { unPP :: a }

instance Pretty a => Show (PP a) where
  show = show . pp . unPP

(<.>) :: Doc -> Doc -> Doc
(<.>) = (<>)
infixl 6 <.>

above, indent :: Doc -> Doc -> Doc
above  a b = hang a 0 b
indent a b = hang a 2 b

infixr 1 `above`
infixl 2 `indent`

color :: (Int, Bool) -> Doc -> Doc
color (c, b) d = zeroWidthText begin <.> d <.> zeroWidthText end
  where
    begin = "\x1b[" ++ show (30 + c) ++ (if b then ";1" else "") ++ "m"
    end   = "\x1b[0m"
