
module Prog where

import Control.Comonad.Cofree
import Control.Lens hiding (Const, (<.>), (:<), (?=))

import Pretty
import Info
import Name

gist :: Cofree f i -> f (Cofree f i)
gist (_ :< f) = f

with :: (i -> f (Cofree f i) -> a) -> Cofree f i -> a
with k (i :< f) = k i f

withGist :: (f (Cofree f i) -> g (Cofree g i)) -> Cofree f i -> Cofree g i
withGist f (i :< fs) = i :< f fs

type Prog = Cofree Prog_ Info
data Prog_ self
  = Var   Name
  | App   self [self]
  | Lam   [(Name, Type)] self
  | If    [IfBranch]
  | Let   [Decl] self
  | Const Const
  | As    self Type
  | Rec   [RecDecl]
  | Get   Prog Name
  | Set   Prog [RecDecl]
  deriving stock (Functor, Foldable, Traversable)

type RecDecl = Cofree RecDecl_ Info
data RecDecl_ self
  = Field   Name Prog
  | Capture Name
  deriving stock (Functor, Foldable, Traversable)

type IfBranch = Cofree IfBranch_ Info
data IfBranch_ self
  = Then Prog Prog
  deriving stock (Functor, Foldable, Traversable)

type Decl = Cofree Decl_ Info
data Decl_ self
  = Decl Name Type Prog
  | Type Name Type
  deriving stock (Functor, Foldable, Traversable)

type Const = Cofree Const_ Info
data Const_ self
  = Bool   Bool
  | Int    Integer
  | Float  Double
  | String String
  | Char   Char
  deriving stock (Functor, Foldable, Traversable)

type Type = Cofree Type_ Info
data Type_ self
  = T       Name
  | Arr     self self
  | Renamed Name self
  | TRec    [(Name, Type)]
  | TVar    Name
  deriving stock (Functor, Foldable, Traversable)

makePrisms ''Prog_
makePrisms ''Const_
makePrisms ''Type_

instance Pretty (f (Cofree f i)) => Pretty (Cofree f i) where
  pp = pp . gist

deriving via PP Const instance Show Const

instance Pretty (Decl_ Decl) where
  pp (Decl n t b) = dnm (pp n) `indent` ko ":" <+> pp t `indent` ko "=" <+> pp b
  pp (Type n t)   = kw "type" <+> dnm (pp n) <+> ko "=" `indent` pp t

instance Pretty (Prog_ Prog)  where
  pp = \case
    Var v     -> nm do pp v
    App f xs  -> pp f `indent` fsep (map ppParen xs)
    If bs     -> kw "if" `indent` vcat (map pp bs) `above` kw "end"
    Lam as b  -> kw "fun" <+> fsep (map ppTy as) <+> kw "=>" `indent` pp b `above` kw "end"
    Const c   -> pp c
    Let ds b  -> kw "let" `indent` vcat (punctuate semi (map pp ds)) `above` (kw "in" $$ nest 2 (pp b) $$ kw "end")
    As p ty   -> parens (pp p `indent` (kw "as" `indent` pp ty))
    Rec fs    -> "{" `indent` vcat (punctuate semi (map pp fs)) `above` "}"
    Get p f   -> parenOn (_App) p (pp p) <.> "." <.> pr (pp f)
    Set p fs  -> ("{" `indent` pp p) `indent` vcat (punctuate semi (map pp fs)) `above` "}"
    where
      ppTy (n, ty) = "(" <.> dnm (pp n) <.> ko ":" `indent` pp ty <.> ")"

      ppParen :: Prog -> Doc
      ppParen x = parenOn _App x (pp x)

instance Pretty (IfBranch_ IfBranch) where
  pp = \case
    Then b y -> ko "|" <+> pp b <+> ko "->" `indent` pp y

instance Pretty (RecDecl_ RecDecl) where
  pp = \case
    Field n p -> pr (pp n) <+> ko "=" `indent` pp p
    Capture n -> pr (pp n)

instance Pretty (Type_ Type) where
  pp = \case
    T       t   -> tty $ pp t
    Arr     a b -> parenOn _Arr a (pp a) `indent` ttu "->" <+> pp b
    Renamed n _ -> tty $ pp n
    TRec    fs  -> "{" `indent` vcat (punctuate semi (map ppFs fs)) `above` "}"
    TVar    n   -> tty $ pp n
    where
      ppFs (n, p)  = pr (pp n) <+> ":" `indent` pp p

instance Pretty (Const_ Const) where
  pp (Bool   b) = if b then con "true" else con "false"
  pp (Int    b) = con $ integer b
  pp (Float  b) = con $ double b
  pp (String b) = con $ doubleQuotes (text b)
  pp (Char   b) = con $ quotes (char b)

parenOn :: APrism' (f (Cofree f i)) a -> Cofree f i -> Doc -> Doc
parenOn p s str | isn't p (gist s) = str
parenOn _ _ str                    = "(" `indent` str `above` ")"

parenOnNot :: APrism' (f (Cofree f i)) a -> Cofree f i -> Doc -> Doc
parenOnNot p s str | isn't p (gist s) = "(" `indent` str `above` ")"
parenOnNot _ _ str                    = str

(\/) :: Prism' s a -> Prism' s b -> Prism' s (Either a b)
l \/ r = prism (either (l #) (r #)) \s -> maybe (maybe (Left s) (Right . Right) (s^?r)) (Right . Left) (s^?l)

kw, nm, con, ko, tty, ttu, dnm, pr :: Doc -> Doc
kw  = color (0, True)
ko  = color (0, True)
con = color (2, False)
tty = color (5, False)
ttu = color (4, False)
nm  = color (7, False)
dnm = color (3, True)
pr  = color (3, False)
